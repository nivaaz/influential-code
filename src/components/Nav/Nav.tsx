import React from "react";
import styles from "./styles.module.css";
import { useAuth, AuthCheck } from "reactfire";
import * as ROUTES from "./../../routes";
import mysvg from "./ProfileWhite.svg";

const Nav: React.FC = () => {
  const auth = useAuth();
  const signOut = async () => {
    await auth.signOut();
  };

  const LoggedIn = () => {
    return (
      <>
        <a href={ROUTES.ACCOUNT}>
          <img className={styles.profile} src={mysvg} alt="profile" />
        </a>
        <a href={ROUTES.HOME}> Influential Codes </a>
        <button className="btn btn-dark" onClick={signOut}>
          Sign Out
        </button>
      </>
    );
  };

  const LoggedOut = () => (
    <>
      <a href={ROUTES.HOME}> Influential Codes </a>
      <a href={ROUTES.SIGN_IN}> Login </a>
    </>
  );

  return (
    <div className={styles.navContainer}>
      <AuthCheck fallback={<LoggedOut />}>
        <LoggedIn />
      </AuthCheck>
    </div>
  );
};

export default Nav;
