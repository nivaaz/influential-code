import React from 'react';

type Props = {
  conter: number;
};
type MyState = {
  count: Number;
};
export class SearchResults extends React.PureComponent<Props, MyState> {
  state: MyState = {
    count: 1,
  };
  render() {
    return <div> {this.state.count} </div>;
  }
}
