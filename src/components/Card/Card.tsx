import React from 'react';
import styles from './card.module.css';

// get dp from the back end

export type CardProps = {
  influencer: string;
  brand: string;
  code: string;
  discount: number;
  proceeds?: string;
  codeType: string;
  isDark: boolean;
};

export const Card = (props: CardProps) => {
  const brandhref = '/profile/brand/' + props.brand.trim();
  const influencerHref = '/profile/influencer/' + props.influencer.trim();

  if (props.isDark) {
    return (
      <div className={styles.cardContainerDark}>
        <div className={styles.topCardContainer}>
          <div className={styles.nameBrandContainer}>
            <a href={influencerHref} className={styles.influencerIcon}>
              {props.influencer[0]}{' '}
            </a>
            <a href={influencerHref} className={styles.influencer}>
              {props.influencer}
            </a>
          </div>

          <div className={styles.discountCodeContainer}>
            <p className={styles.code}> {props.code} </p>
            <p className={styles.discount}> {props.discount}% </p>
          </div>

          <div className={styles.brandContainer}>
            <a href={brandhref} className={styles.brand}>
              {props.brand}
            </a>
          </div>
          <div className={styles.nameBrandContainer}>
            <p className={styles.codeType}> {props.codeType[0]} </p>
            {props.proceeds ? <p className={styles.proceeds}> {props.proceeds} </p> : <> </>}
          </div>
        </div>
      </div>
    );
  }
  return (
    <div className={styles.cardContainer}>
      <div className={styles.topCardContainer}>
        <div className={styles.nameBrandContainer}>
          <a href={influencerHref} className={styles.influencerIcon}>
            {props.influencer[0]}{' '}
          </a>
          <a href={influencerHref} className={styles.influencer}>
            {props.influencer}
          </a>
        </div>

        <div className={styles.discountCodeContainer}>
          <p className={styles.code}> {props.code} </p>
          <p className={styles.discount}> {props.discount}% </p>
        </div>

        <div className={styles.brandContainer}>
          <a href={brandhref} className={styles.brand}>
            {props.brand}
          </a>
        </div>
        <div className={styles.nameBrandContainer}>
          <p className={styles.codeType}> {props.codeType[0]} </p>
          {props.proceeds ? <p className={styles.proceeds}> {props.proceeds} </p> : <> </>}
        </div>
      </div>
    </div>
  );
};
