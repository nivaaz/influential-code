import React, { useState } from "react";
import { withRouter } from "react-router-dom";
import * as ROUTES from "../../routes";
import styles from "./signup.module.css";
import { useAuth } from "reactfire";

export const SignUpPage: React.FC = () => (
  <>
    <h2>Sign Up</h2>
    <SignUpForm />
  </>
);

const INITIAL_STATE = {
  email: "",
  passwordOne: "",
  passwordTwo: "",
};

const SignUpFormBase = (props: any) => {
  const [userInput, setUserInput] = useState(INITIAL_STATE);
  const [error, setError] = useState("");
  const auth = useAuth();

  const onSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const { email, passwordOne } = userInput;

    try {
      const authUser = await auth.createUserWithEmailAndPassword(email, passwordOne);
      setUserInput({ ...INITIAL_STATE });
      console.log("authUser", authUser);
      // Redirect the user to another page, a protected route for only authenticated users.
      props.history.push(ROUTES.HOME);
    } catch (error) {
      // TODO: check the error.code to provide a better experience on failure cases
      console.log(error);
      setError(error.message);
    }
  };

  const onChange = (e: any) => {
    setUserInput({ ...userInput, [e.target.name]: e.target.value });
  };

  const { email, passwordOne, passwordTwo } = userInput;

  const isInvalid = passwordOne !== passwordTwo || passwordOne === "" || email === "";

  return (
    <form onSubmit={onSubmit}>
      <label> Email</label>
      <input
        type="text"
        name="email"
        value={email}
        onChange={onChange}
        placeholder="Email Address"
      />
      <label> Password</label>

      <input
        type="password"
        name="passwordOne"
        value={passwordOne}
        onChange={onChange}
        placeholder="Password"
      />
      <label> Confirm Password</label>

      <input
        type="password"
        name="passwordTwo"
        value={passwordTwo}
        onChange={onChange}
        placeholder="Confirm Password"
      />
      <button disabled={isInvalid} type="submit" className="CTA">
        Sign Up
      </button>
      {error && <p className="error">{error}</p>}
      <SignInLink/>
    </form>
  );
};

const SignUpForm = withRouter(SignUpFormBase);

const SignInLink: React.FC = () => (
  <a className={styles.link} href={ROUTES.SIGN_IN}>
    Have an account? Sign in
  </a>
);
