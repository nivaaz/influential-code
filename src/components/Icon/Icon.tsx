import React from "react";
import styles from "./styles.module.css";

type Props = { src: string }; /* could also use interface */

const Icon: React.FC<Props> = (props: Props) => {

  return (
    <div className={styles.iconbox}>
        <img  src={props.src} alt="icon" />
    </div>
    );
};

export default Icon;
