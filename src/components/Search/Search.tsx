import React from "react";
import styles from "./search.module.css";
import data from "../../TestData/card.js";
import { Card } from "../Card/Card";

type State = {
  searchString: string;
};
type Props = {
  proper?: string;
};
export class Search extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      searchString: "",
    };
  }
  handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    this.setState({ searchString: e.currentTarget.value });
    console.log(this.state.searchString);
  };
  renderData = () => {
    return data.map((key, index) => {
      const searchString = this.state.searchString;
      if (
        searchString !== "" &&
        (key.influencer.includes(searchString) ||
          key.brand.includes(searchString) ||
          key.code.includes(searchString) ||
          key.discount.includes(searchString) ||
          key.proceeds.includes(searchString))
      ) {
        return (
          <Card
            key={index}
            influencer={key.influencer}
            brand={key.brand}
            code={key.code}
            discount={Number(key.discount)}
            proceeds={key.proceeds}
            codeType={key.codeType}
            isDark={true}
          />
        );
      } else {
        return <> </>;
      }
    });
  };
  render() {
    return (
      <div>
        <div className={styles.searchContainer}>
          <input type="search" placeholder="🔎 Start searching" onChange={this.handleChange} className={styles.searchBar} />
        </div>
        <div className={styles.searchResults}>{this.renderData()}</div>
      </div>
    );
  }
}
