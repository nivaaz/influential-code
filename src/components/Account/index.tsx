import React from "react";
import styles from "./styles.module.css";
import ChangePassword from "../ChangePassword";
import PasswordForgetPage from "../ForgotPassword";

const Account: React.FC = () => (
  <div className={styles.accountpage}>
    <h2>Account Page</h2>
    <EditDisplayName />
    <EditBio />
    <PasswordForgetPage />
    <ChangePassword />
  </div>
);

const EditDisplayName = () => {
  return (
    <div className={styles.page}>
      <label> Display name</label>
      <input defaultValue="display name" />
    </div>
  );
};
const EditBio = () => {
  return (
    <div className={styles.page}>
      <label> Edit Bio</label>
      <input defaultValue="edit bio" />
    </div>
  );
};

export default Account;
