import React, { useState } from "react";
import { useAuth, AuthCheck } from "reactfire";

const INITIAL_STATE = {
  passwordOne: "",
  passwordTwo: "",
};

const ChangePassword = () => {
  const [userInput, setUserInput] = useState(INITIAL_STATE);
  const [error, setError] = useState("");
  const [success, setSuccess] = useState(false);
  const auth = useAuth();

  const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const { passwordOne } = userInput;
    try {
      await auth.currentUser?.updatePassword(passwordOne);
      setUserInput({ ...INITIAL_STATE });
      setSuccess(true);
    } catch (error) {
      // TODO: check the error.code to provide a better experience on failure cases
      console.log(error);
      setError(error.message);
    }
  };

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setUserInput({ ...userInput, [e.target.name]: e.target.value });
  };

  const { passwordOne, passwordTwo } = userInput;

  const isInvalid = passwordOne !== passwordTwo || passwordOne === "";

  return (
    <>
      <AuthCheck fallback={<p> You are not logged in!! </p>}>
        <h2> Change password </h2>

        <form onSubmit={onSubmit}>
          <label> New password </label>
          <input
            type="password"
            name="passwordOne"
            value={passwordOne}
            onChange={onChange}
            placeholder="New Password"
          />
          <label> Confirm new password </label>
          <input
            type="password"
            name="passwordTwo"
            value={passwordTwo}
            onChange={onChange}
            placeholder="Confirm New Password"
          />
          <button disabled={isInvalid} type="submit">
            Change Password
          </button>
          {error && <p>{error}</p>}
          {success && <p>Your password has been changed successfully.</p>}
        </form>
      </AuthCheck>
    </>
  );
};

export default ChangePassword;
