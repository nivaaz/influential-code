import React, { useState } from "react";
import styles from "./styles.module.css";
import * as ROUTES from "../../routes";
import { useAuth } from "reactfire";

const PasswordForgetPage = () => {
  return (
    <>
      <h2> Reset Password </h2>
      <PasswordForgetForm />
    </>
  );
};

const PasswordForgetForm = () => {
  const [email, setEmail] = useState("");
  const [error, setError] = useState("");
  const auth = useAuth();

  const onSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    try {
      await auth.sendPasswordResetEmail(email);
      setEmail("");
    } catch (error) {
      // TODO: check the error.code to provide a better experience on failure cases
      console.log(error);
      setError(error.message);
    }
  };

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(e.target.value);
  };

  const isInvalid = email === "";

  return (
    <form onSubmit={onSubmit}>
      <p> Enter your email below to get an email to reset your password.</p>
      <label> Email </label>
      <input
        type="text"
        name="email"
        value={email}
        onChange={onChange}
        placeholder="Email Address"
      />
      <button disabled={isInvalid} type="submit">
        Reset Password
      </button>
      {error && <p>{error}</p>}
    </form>
  );
};

const PasswordForgetLink = () => (
  <a className={styles.link} href={ROUTES.PASSWORD_FORGET}>
    Forgot Password?
  </a>
);

export default PasswordForgetPage;

export { PasswordForgetForm, PasswordForgetLink };
