import React, { useState } from "react";
import * as ROUTES from "../../routes";
import { withRouter } from "react-router-dom";
import styles from "./signin.module.css";
import { useAuth } from "reactfire";
import { PasswordForgetLink } from "../ForgotPassword";

export const SignInPage: React.FC = () => (
  <>
    <h2>SignIn</h2>
    <SignInForm />
  <div className={styles.center}>
    <PasswordForgetLink />
    </div>
  </>
);

const INITIAL_STATE = {
  email: "",
  password: "",
};

const SignInFormBase = (props: any) => {
  const [userInput, setUserInput] = useState(INITIAL_STATE);
  const [error, setError] = useState("");
  const auth = useAuth();

  const onSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const { email, password } = userInput;

    try {
      await auth.signInWithEmailAndPassword(email, password);
      setUserInput({ ...INITIAL_STATE });
      props.history.push(ROUTES.HOME);
    } catch (error) {
      // TODO: check the error.code for two cases:
      // 1. user not found: show message and prompt to redirect to sign up page
      // 2. login failure: show message and ask them to try again later
      // 3. password incorrect: show password incorrect message, red outline on password input
      console.log(error);
      setError(error.message);
    }
  };

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setUserInput({ ...userInput, [e.target.name]: e.target.value });
  };

  const { email, password } = userInput;

  const isInvalid = password === "" || email === "";

  return (
    <form className={styles.signIn}  onSubmit={onSubmit}>
      <label> Email </label>
      <input
        name="email"
        value={email}
        onChange={onChange}
        type="text"
        placeholder="Email Address"
      />
      <label> Password </label>
      <input
        name="password"
        value={password}
        onChange={onChange}
        type="password"
        placeholder="Password"
      />
      <button disabled={isInvalid} type="submit" className="CTA">
        Sign In
      </button>
      {error && <p>{error}</p>}
      <SignUpLink/>
    </form>
  );
};

const SignInForm = withRouter(SignInFormBase);

const SignUpLink: React.FC = () => (
  <a className={styles.link} href={ROUTES.SIGN_UP}>
    Don't have an account? Sign Up
  </a>
);
