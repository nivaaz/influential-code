import React from 'react';
import styles from './socialmedia.module.css';
import { library, IconName } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  fab,
  faFacebook,
  faInstagram,
  faTwitter,
  faSnapchat,
} from '@fortawesome/free-brands-svg-icons';

library.add(fab, faFacebook, faInstagram, faTwitter, faSnapchat);

type Props = {
  cards: SocialMediaProps[];
};
type SocialMediaProps = {
  socialMedia: IconName;
  link?: string;
  userName: string;
  onClickHandle?: (event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => void;
};

export class SocialMedia extends React.PureComponent<Props> {
  render() {
    return (
      <div className={styles.socialContainer}>
        {this.props.cards.map((key: SocialMediaProps) => {
          return singleSocial(key);
        })}
      </div>
    );
  }
}

const singleSocial = (props: SocialMediaProps) => {
  if (props.link) {
    return (
      <a href={props.link} className={styles.socialCard}>
        <FontAwesomeIcon icon={['fab', props.socialMedia]} />
      </a>
    );
  } else if (props.onClickHandle) {
    return (
      <a href={props.link} onClick={props.onClickHandle} className={styles.socialCard}>
        <FontAwesomeIcon icon={['fab', props.socialMedia]} />
      </a>
    );
  } else return <></>;
};
