import React, { useEffect } from "react";
import { BrowserRouter, Route, Switch, useLocation } from "react-router-dom";
import { FirebaseAppProvider, SuspenseWithPerf } from "reactfire";
import { useAnalytics } from "reactfire";
import "./App.css";
import PasswordForgetPage from "./components/ForgotPassword";
import Nav from "./components/Nav/Nav";
import { SignInPage } from "./components/SignIn";
import { firebaseConfig } from "./firebase/firebase";
import { AboutUs } from "./Pages/AboutUs/AboutUs";
import { AddCode } from "./Pages/AddCode/AddCode";
import { CreateAccount } from "./Pages/CreateAccount/CreateAccount";
import { ErrorPage } from "./Pages/ErrorPage/ErrorPage";
import { PersonalPage } from "./Pages/PersonalPage/PersonalPage";
import { SearchPage } from "./Pages/SearchPage/SearchPage";
import * as ROUTES from "./routes";
import { SignUpPage } from "./components/SignUp/SignUp";
import ChangePassword from "./components/ChangePassword";

const MyPageViewLogger = () => {
  const location = useLocation();
  // By passing `location.pathname` to the second argument of `useEffect`,
  // we only log on first render and when the `pathname` changes
  const analytics = useAnalytics();
  useEffect(() => {
    analytics.logEvent("page-view", { path_name: location.pathname });
  }, [location.pathname, analytics]);
  return null;
};

const App = () => {
  return (
    <>
      <FirebaseAppProvider firebaseConfig={firebaseConfig} initPerformance suspense={true}>
        <SuspenseWithPerf fallback={<Loading />} traceId={"loading-app-status"}>
          <Nav />
          <BrowserRouter>
            <Switch>
              <Route exact path={ROUTES.LANDING} component={SearchPage} />
              <Route exact path={ROUTES.EDITACCOUNT} component={CreateAccount} />
              {/* <Route exact path={ROUTES.ACCOUNT} component={Account} /> */}
              <Route exact path={ROUTES.SIGN_IN} component={SignInPage} />
              <Route exact path={ROUTES.SIGN_UP} component={SignUpPage} />
              <Route exact path={ROUTES.PASSWORD_FORGET} component={PasswordForgetPage} />
              <Route exact path={ROUTES.PASSWORD_RESET} component={ChangePassword} />
              {/* <Route exact path="/edit/:brandType/:brandName/" component={EditPage} /> */}
              <Route exact path="/addCodes" component={AddCode} />
              <Route exact path="/AboutUs" component={AboutUs} />
              <Route exact path="/profile/:brandType/:brandName/" component={PersonalPage} />
              <Route path="*" component={ErrorPage} />
            </Switch>
            <MyPageViewLogger />
          </BrowserRouter>
        </SuspenseWithPerf>
      </FirebaseAppProvider>
    </>
  );
};

const Loading = () => {
  return (
    <div className="loading">
      <h1> Loading </h1>
    </div>
  );
};
export default App;
