import React from "react";
import styles from "./addCode.module.css";

type Props = {
  getAccountDetails?: () => void; // to be wrriten
};

type State = {
  discountType: "$" | "%" | string;
  code: string;
  discount: number | string;
  brand: string;
  websiteLink: string;
  codeType: "Donation" | "Comission" | "None";
  hasExpiry: "Yes" | "No";
  expiryDate?: Date;
  step: number;
  affiliate: boolean;
  hasComission?: boolean;
  donation?: string;
  link?: string;
};

export class AddCode extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      discountType: "$",
      code: "",
      discount: 0,
      brand: "",
      websiteLink: "string",
      codeType: "None",
      hasExpiry: "No",
      step: 0,
      affiliate: false,
      hasComission: false,
      donation: "",
      link:"",

    };
  }
  onChangeUpdate = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const stateid = e.currentTarget.name;
    const newVal: boolean | string = e.currentTarget.value;
    this.setState(({
      [stateid]: newVal,
    } as unknown) as Pick<State, keyof State>);
  };
  onClickUpdate = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>): void => {
    const stateid = e.currentTarget.name;
    let newVal: boolean | string  = e.currentTarget.value;
    console.log(stateid, newVal);
    if (stateid === "affiliate") {
      newVal = e.currentTarget.value === "true";
    }
    if (stateid === "hasComission") {
      newVal = e.currentTarget.value === "true";
    }
    this.setState(({
      [stateid]: newVal,
    } as unknown) as Pick<State, keyof State>);
  };
  renderExpiry = () => {
    if (this.state.hasExpiry === "Yes") {
      return (
        <div>
          <p> When does the code expire?</p>
          <input type="date" min="2020-01-01" name="expiryDate" onChange={this.onChangeUpdate} />
        </div>
      );
    } else return <> </>;
  };
  render() {
    return (
      <div className={styles.addCodeContainer}>
        <h2> Add a code</h2>
        <CodeType affiliate={this.state.affiliate} onClickUpdate={this.onClickUpdate} />
        <DiscountType
          onClickUpdate={this.onClickUpdate}
          onChange={this.onChangeUpdate}
          discountType={this.state.discountType}
        />
        <CodeExpiry onClickUpdate={this.onClickUpdate} hasExpiry={this.state.hasExpiry} />
        {this.renderExpiry()}
        <Partnership onClickUpdate={this.onClickUpdate} onChange={this.onChangeUpdate} partnership = {this.state.affiliate} />
        <Comission
          onClickUpdate={this.onClickUpdate}
          codeType={this.state.codeType}
          hasComission={this.state.hasComission}
          onChangeUpdate={this.onChangeUpdate}
        />
        <button> ADD CODE </button>
      </div>
    );
  }
}

const CodeType = ({
  onClickUpdate,
  affiliate,
}: {
  affiliate: boolean;
  onClickUpdate: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}): React.ReactElement => {
  return (
    <>
      <h1> Code type</h1>
      <p> Is this a discount code or affilaite link? </p>
      <button
        value="false"
        name="affiliate"
        className={affiliate === false ? styles.selected : ""}
        onClick={onClickUpdate}
      >
        Non affiliate Code
      </button>
      <button
        value="true"
        name="affiliate"
        className={affiliate === true ? styles.selected : ""}
        onClick={onClickUpdate}
      >
        Affilaite Link
      </button>
    </>
  );
};
const Comission = ({
  onClickUpdate,
  onChangeUpdate,
  codeType,
  hasComission
}: {
  onClickUpdate: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  onChangeUpdate: (e: React.ChangeEvent<HTMLInputElement>) => void;
  codeType: "Donation" | "Comission" | "None";
  hasComission?: boolean;
}): React.ReactElement => {
  return (
    <>
      <h1> Comission </h1>
      <div>
        <p> Is there a comission attached?</p>
        <button 
          name="hasComission"
          value="true" 
          onClick={onClickUpdate}
          className={!!hasComission ? styles.selected : ""}

          > Yes </button>
        <button
           name="hasComission"
           value="false"
           onClick={onClickUpdate}
          className={!hasComission  ? styles.selected : ""}

          > No </button>
      </div>

      <div>
        <p> Where does the comission go?</p>
        <button
          className={codeType === "Donation" ? styles.selected : ""}
          onClick={onClickUpdate}
          name="codeType"
          value="Donation"
        >
          Donation
        </button>

        <button
          className={codeType === "Comission" ? styles.selected : ""}
          onClick={onClickUpdate}
          name="codeType"
          value="Comission"
        >
          Comission
        </button>

        <button
          className={codeType === "None" ? styles.selected : ""}
          onClick={onClickUpdate}
          name="codeType"
          value="None"
        >
          None
        </button>
      </div>
      {codeType !== 'None' && 
      (<div>
        <p> Where is the {codeType.toLocaleLowerCase()} sent to?</p>
        <input type="text" placeholder="Starlight foundation" onChange={onChangeUpdate} />
        <p> Provide a link to their website </p>
        <input type="url" placeholder="www.starlightfoundation.com.au" onChange={onChangeUpdate} />
      </div>)}
    </>
  );
};

const DiscountType = ({
  onClickUpdate,
  discountType,
  onChange,
}: {
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onClickUpdate: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  discountType: string;
}): React.ReactElement => {
  return (
    <>
      <h1> Discount</h1>
      <p> Is there a discount or deal associated with it? </p>
      <button 
      className={discountType === "$" ? styles.selected : ""}
      name="discountType" value="$" onClick={onClickUpdate}> Cash discount </button>
      <button 
      className={discountType === "%" ? styles.selected : ""}
      name="discountType" value="%" onClick={onClickUpdate}> Percentage discount </button>
      <button 
      className={discountType === "deal" ? styles.selected : ""}
      name="discountType" value="deal" onClick={onClickUpdate}> Deal </button>
      <button 
      className={discountType === "none" ? styles.selected : ""}
      name="discountType" value="none" onClick={onClickUpdate}> No </button>

      {discountType === "percentage" ? (
        <div>
          <p> How much is the percentage discount? </p>
          <input
            name="discount"
            type="text"
            placeholder="30% off any order over $100"
            onChange={onChange}
          />
        </div>
      ) : (
        <></>
      )}

      {discountType === "cash" ? (
        <div>
          <p> How much is the cash discount? </p>
          <input name="discount" type="text" placeholder="$10 off any order" onChange={onChange} />
        </div>
      ) : (
        <></>
      )}

      {discountType === "deal" ? (
        <div>
          <p> What is the deal? </p>
          <input
            name="discount"
            type="text"
            placeholder="3 for the price of 1"
            onChange={onChange}
          />
        </div>
      ) : (
        <></>
      )}
    </>
  );
};

const CodeExpiry = ({
  onClickUpdate,
  hasExpiry,
}: {
  hasExpiry: string;
  onClickUpdate: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}): React.ReactElement => {
  return (
    <>
      <h1> Code expiry </h1>
      <div>
        <p> Does this code expire?</p>
        <button
          className={hasExpiry === "Yes" ? styles.selected : ""}
          onClick={onClickUpdate}
          name="hasExpiry"
          value="Yes"
        >
          {" "}
          Yes
        </button>
        <button
          className={hasExpiry === "No" ? styles.selected : ""}
          onClick={onClickUpdate}
          name="hasExpiry"
          value="No"
        >
          {" "}
          No
        </button>
      </div>
    </>
  );
};

const Partnership = ({
  onClickUpdate,
  onChange,
  partnership
}: {
  partnership: boolean;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onClickUpdate: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}): React.ReactElement => {
  return (
    <>
      <h1> Partnerships </h1>
      <div>
        <p> Do you have a paid partnership with this brand? </p>
        <button 
        className = {partnership === true ? styles.selected: ""}
        name="affiliate" value="true" onClick={onClickUpdate}> Yes </button>
        <button 
        className = {partnership === false ? styles.selected: ""}
        name="affiliate" value="false" onClick={onClickUpdate}> No </button>
      </div>
      <h1> The code </h1>
      <div>
        <p> What is the code?</p>
        <input placeholder="CODE30" name="code" type="text" onChange={onChange} />
      </div>
      <div>
        <p> What is the affiliate link?</p>
        <input
          placeholder="influentialcodes.com/CODE30"
          name="code"
          type="code"
          onChange={onChange}
        />
        <div>
          <p> What brand is it connected to?</p>
          <input placeholder="TESLA" name="brand" type="text" onChange={onChange} />
        </div>

        <div>
          <p> Do you have an affiliate link for the website?</p>
          <input
            placeholder="influentialcodes.com/CODE30"
            name="websiteLink"
            type="text"
            onChange={onChange}
          />
        </div>
      </div>
    </>
  );
};
