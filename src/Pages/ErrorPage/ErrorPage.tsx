import React from 'react';
import { Card } from '../../components/Card/Card';
import styles from './errorPage.module.css'

export const ErrorPage: React.FunctionComponent = () => {
    return (
    <div className={styles.errorPageContainer}>
        <h1> Oops.</h1>
        <p> I don't think you wanted this code. </p>
        <Card
        influencer = "Oops"
        brand= "NOTFOUND"
        code= "404"
        discount= {-40}
        proceeds= "proceeds don't go to dogs"
        codeType= "A"
        isDark= {false}
        />

        <p> Let's get you back to the discounts.</p>

        <a href="/" className={styles.backButton}> Back to search</a>
        </div>
    )
};
