import React from 'react';
import styles from './createAccount.module.css';
import { IconName } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

type Props = {
  getAccountDetails?: () => void; // to be wrriten
};
// type socials = {
//   facebook?: boolean;
//   instagram?: boolean;
//   snapchat?: boolean;
//   twitter?: boolean;
// };
type State = {
  name: string;
  profileType: 'brand' | 'influencer';
  bio: string;
  facebook: 'facebook' | '';
  instagram: 'instagram' | '';
  snapchat: 'snapchat' | '';
  twitter: 'twitter' | '';
  socials?: SocialMediaProps[];
};

type SocialMediaProps = {
  socialMedia: IconName;
  link?: string;
  userName: string;
  onClickHandle?: (event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => void;
};

export class CreateAccount extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      name: 'string',
      profileType: 'brand',
      bio: '',
      facebook: '',
      instagram: '',
      snapchat: '',
      twitter: '',
    };
  }
  onChangeUpdate = (e: React.ChangeEvent<HTMLInputElement> |  React.ChangeEvent<HTMLTextAreaElement>): void => {
    const stateid = e.currentTarget.name;
    const newVal = e.currentTarget.value;
    this.setState(({
      [stateid]: newVal,
    } as unknown) as Pick<State, keyof State>);
  };
  renderAddSocial = (socialType: 'facebook' | 'instagram' | 'snapchat' | 'twitter') => {
    if (this.state[socialType]) {
      return (
        <div>
          <h3> Add {socialType} </h3>
          <p> Username </p>
          <input name="socials" id={socialType} onChange={this.onChangeUpdate} />
        </div>
      );
    } else return <></>;
  };
  onClickUpdate = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>): void => {
    const stateid = e.currentTarget.name;
    const newVal = e.currentTarget.value;
    console.log(stateid, newVal);
    if (
      stateid === 'facebook' ||
      stateid === 'snapchat' ||
      stateid === 'instagram' ||
      stateid === 'twitter'
    ) {
      console.log(stateid === this.state[stateid]);
      if (stateid === this.state[stateid]) {
        console.log(stateid, 'this.state[stateid]');
        this.setState(({
          [stateid]: '',
        } as unknown) as Pick<State, keyof State>);
      } else {
        this.setState(({
          [stateid]: newVal,
        } as unknown) as Pick<State, keyof State>);
      }
    } else {
      this.setState(({
        [stateid]: newVal,
      } as unknown) as Pick<State, keyof State>);
    }
  };
  render() {
    return (
      <div className={styles.addCodeContainer}>
        <h2> Create Account </h2>
        <div className={styles.section}>
          <p>Select the account type</p>
          <button
            className={this.state.profileType === 'brand' ? styles.selected : ''}
            onClick={this.onClickUpdate}
            name="profileType"
            value="brand"
          >
            Brand
          </button>

          <button
            className={this.state.profileType === 'influencer' ? styles.selected : ''}
            name="profileType"
            onClick={this.onClickUpdate}
            value="influencer"
          >
            Influencer
          </button>
        </div>
        <div className={styles.section}>

          <p> Account name</p>
          <input name="name" onChange={this.onChangeUpdate} />
        </div>
        <div className={styles.section}>

          <p> Add a bio </p>
          <textarea name="bio"
          cols={40} rows={5}
          onChange={this.onChangeUpdate}
          ></textarea>
        </div>
        <div className={styles.section}>

          <p> Select Social Media you would like to add </p>

          <button
            className={this.state.facebook === 'facebook' ? styles.selected : ''}
            onClick={this.onClickUpdate}
            value="facebook"
            name="facebook"
          >
            <FontAwesomeIcon icon={['fab', 'facebook']} />
          </button>

          <button
            className={this.state.instagram === 'instagram' ? styles.selected : ''}
            onClick={this.onClickUpdate}
            value="instagram"
            name="instagram"
          >
            <FontAwesomeIcon icon={['fab', 'instagram']} />
          </button>

          <button
            className={this.state.snapchat === 'snapchat' ? styles.selected : ''}
            onClick={this.onClickUpdate}
            value="snapchat"
            name="snapchat"
          >
            <FontAwesomeIcon icon={['fab', 'snapchat']} />
          </button>

          <button
            className={this.state.twitter === 'twitter' ? styles.selected : ''}
            onClick={this.onClickUpdate}
            value="twitter"
            name="twitter"
          >
            <FontAwesomeIcon icon={['fab', 'twitter']} />
          </button>
        </div>
        {this.renderAddSocial('facebook')}
        {this.renderAddSocial('instagram')}
        {this.renderAddSocial('twitter')}
        {this.renderAddSocial('snapchat')}

        <button className={styles.createAccount}> Create Account </button>
      </div>
    );
  }
}
