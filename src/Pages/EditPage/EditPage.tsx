import React from "react";
import { CardProps, Card } from "../../components/Card/Card";
import styles from "./editpage.module.css";
import { Search } from "../../components/Search/Search";
import { SocialMedia } from "../../components/SocialMedia/SocialMedia";
import { IconName } from "@fortawesome/fontawesome-svg-core";
import { useParams } from "react-router-dom";
import cards from "../../TestData/card.js";
import Nav from "../../components/Nav/Nav";

type Props = {
  pageType?: string;
  pageName?: string;
  cards?: CardProps[];
};

type SocialMediaProps = {
  socialMedia: IconName;
  link: string;
  userName: string;
};

const socials: SocialMediaProps[] = [
  {
    socialMedia: "facebook",
    link: "facebook.com",
    userName: "@nivaaz",
  },
  {
    socialMedia: "instagram",
    link: "insatgram.com",
    userName: "@zaavin",
  },
  {
    socialMedia: "snapchat",
    link: "facebook.com",
    userName: "@djvazzie",
  },
  {
    socialMedia: "twitter",
    link: "twitter.com",
    userName: "@thenewthingg",
  },
];

// need to add state.
export const EditPage: React.FunctionComponent<Props> = () => {
  const { brandType, brandName } = useParams();

  const profileType = brandType === "influencer" ? "influencer" : "brand";

  const renderCards = () => {
    const retcards = cards.map((key, i) => {
      if (key[profileType].trim() === brandName) {
        return (
          <div style={{ display: "flex" }}>
            <Card
              influencer={key.influencer}
              brand={key.brand}
              discount={Number(key.discount)}
              code={key.code}
              proceeds={key.proceeds}
              codeType={key.codeType}
              isDark={false}
            />
            <div>
              <button value={i}> edit </button>
              <button value={i}> X </button>
            </div>
          </div>
        );
      } else {
        return <> </>;
      }
    });
    return retcards;
  };

  return (
    <div className={styles.personalPage}>
      <Nav />
      <div className={styles.pageHeader}>
        <Search />
        <h1> {brandName} </h1>
        <SocialMedia cards={socials} />
      </div>

      <div className={styles.cardsList}>{renderCards()}</div>
    </div>
  );
};
