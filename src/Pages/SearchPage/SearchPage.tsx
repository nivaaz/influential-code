import React from 'react';
import { Search } from '../../components/Search/Search';
import styles from './searchPage.module.css';

type Props = {
  nothing?: string;
};
type State = {
  searchType: 'brand' | 'influencer' | 'code', 
};

export class SearchPage extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      searchType: 'code',
    };
  }
  onClickUpdate = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>): void => {
    const stateid = e.currentTarget.name;
    const newVal = e.currentTarget.value;
    console.log(stateid, newVal);

    this.setState(({
      [stateid]: newVal,
    } as unknown) as Pick<State, keyof State>);
  };
 
  render() {
    return (
      <div className={styles.searchPage}>
        <div className={styles.searchContainer}>
          <div className={styles.finda}>
            <h3 className={styles.find}> Find a </h3>
              <button
                className={this.state.searchType === 'brand' ? styles.selected : ''}
                onClick={this.onClickUpdate}
                name="searchType"
                value="brand"
              >
                brand
              </button>
              <button
                className={this.state.searchType === 'influencer' ? styles.selected : ''}
                onClick={this.onClickUpdate}
                name="searchType"
                value="influencer"
              >
                influencer
              </button>
              <h3 className={styles.searchTitle}> or </h3>
              <button
                className={this.state.searchType === 'code' ? styles.selected : ''}
                onClick={this.onClickUpdate}
                name="searchType"
                value="code"
              >
                code
              </button>
              </div>
          <Search />
        </div>
      </div>
    );
  }
}
