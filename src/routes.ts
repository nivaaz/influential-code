export const SIGN_UP = "/sign_up";
export const SIGN_IN = "/sign_in";
export const LANDING = "/";
export const HOME = "/home";
export const ACCOUNT = "/account";
export const EDITACCOUNT = "/edit_account";
export const PASSWORD_FORGET = "/password_forget";
export const PASSWORD_RESET = "/password_reset";
