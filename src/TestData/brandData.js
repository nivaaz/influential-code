module.exports = [
  {
    brand: "SAMSUNG",
    socialMedia: [
      {
        socialMedia: "instagram",
        link: "instagram.com/samsungau",
        userName: "samsungau",
      },
      {
        socialMedia: "facebook",
        link: "facebook.com/samsungau",
        userName: "samsungau",
      },
    ],
  },
  {
    brand: "MORPHE",
    socialMedia: [
      {
        socialMedia: "instagram",
        link: "instagram.com/morphebrushes",
        userName: "morphebrushes",
      },
      {
        socialMedia: "twitter",
        link: "twitter.com/morphebrushes",
        userName: "morphebrushes",
      },
    ],
  },
  {
    brand: "ECHT",
    socialMedia: [
      {
        socialMedia: "instagram",
        link: "instagram.com/echt",
        userName: "echt",
      },
      {
        socialMedia: "twitter",
        link: "twitter.com/echt",
        userName: "echt",
      },
    ],
  },
];
